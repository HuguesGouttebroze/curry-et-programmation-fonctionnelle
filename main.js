import './style.css'
import javascriptLogo from './javascript.svg'
import { setupCounter } from './counter.js'

/**
 * curry multiply fnc
 */

const multiply = (a , b) => a * b;

function curry (fnc) {
  return function (a) {
    return function (b) {
      return fnc(a, b)
    }
  }
}
//items.map(item => multiply(item, 2))
//items.map(curryMultiply(2))
document.body.innerHTML = 
`<div>
<h2>Principe de PROGRAMMATION FNC</h2>
<ul>
  <li>On créé un tableau
    <pre><code>
      const items = [1, 3, 6, 8, 12, 16, 22, 33, 55, 66, 88]
    </code></pre>
  </li>
  <li>Puis une fnc curry
    <pre><code>
      function curry (fnc) {
        return function (a) {
          return function (b) {
            return fnc(a, b)
          }
        }
      }
    </code></pre>
  </li>
  <li>  
  Voici un fnc de multiplication générique, *10  
    <pre><code>
      items.map(curry(multiply)(10)));
    </code></pre>
  </li>
    <li>  
  Ns modifions cette fnc facilement, *2 
    <pre><code>
        items.map(curry(multiply)(2))
    </code></pre>
  </li>
  <li>  
    Autre exemple, avec un parceInt, imaginons un tableau de chaine de characteres, avec qques erreurs, que l'on veux transformer un type number
    <pre><code>
        const items = ['1', '3', 't6', '8', '12u']
    </code></pre>
  </li>
  <li>  
    Autre exemple, avec un parceInt, imaginons un tableau de chaine de characteres, avec qques erreurs, que l'on veux transformer un type number
    <pre><code>
      console.log(
        const items = ['1', '3', 't6', '8', '12u']
      );      console.log(
        items.map(curry(multiply)(2))
      );
    </code></pre>
  </li>
  <li>  
    Ici, on demande pr chaque item un parseInt
    <pre><code>
      items.map(curry(parseInt));
    </code></pre>
  </li>
  <li>  
    Voici le résultat, on remarque que 3 des 5 items n'ont pas pu être convertis en type number.
    <pre><code>
      
      // resultat 
      // items = [1, NaN, NaN, NaN, 6]

    </code></pre>
    parceInt attent un scd paramètre qui est la base
    Le probleme est que ".map" renvoie 2 paramètre, l'élément et l'index.
    Ici, l'idnex est utilisé en tant que base. 
    <li>On peut imaginer que notre fnc est interprété ainsi :  
    <pre><code>
      
      items.map((item, index) => parseInt(item,))

    </code></pre>
  </li>
  </li>
  <li>  
    Voici le résultat, on remarque que 3 des 5 items n'ont pas pu être convertis en type number.
    <pre><code>
      
      // resultat 
      // items = [1, NaN, NaN, NaN, 6]

    </code></pre>
  </li>
  <li>  
    Autre exemple, avec un parceInt, imaginons un tableau de chaine de characteres, avec qques erreurs, que l'on veux transformer un type number
    <pre><code>
      console.log(

      );
    </code></pre>
  </li>
  <li>

  </li>
</ul>
</div>
`

const items = [1, 3, 6, 8, 12, 16, 22, 33, 55, 66, 88]
console.log(items.map(curry(multiply)(10)));
const i = ['1', '3', 't6', '8', '12u']
console.log(i.map(parseInt));
document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="/vite.svg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Hello Vite!</h1>
    <div class="card">
      <button id="counter" type="button"></button>
    </div>
    <p class="read-the-docs">
      Click on the Vite logo to learn more
    </p>
  </div>
`

setupCounter(document.querySelector('#counter'))
